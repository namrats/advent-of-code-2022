import click
from aocd import get_data
from .calorie_counting import CalorieCounting

SOLVERS = [CalorieCounting]

@click.command()
@click.argument('day', type=click.IntRange(min=1, max=1))
def main(day):
    text = get_data(year=2022, day=day)

    solver = SOLVERS[day-1]()
    print(f"Part 1: {solver.part1(text)}")
    print(f"Part 2: {solver.part2(text)}")
