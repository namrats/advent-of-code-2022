def parse_elves(text):
    return [
        [int(item) for item in elf.split()]
        for elf in text.split('\n\n')
    ]

def max_elf_calories(elves):
    return max([sum(elf) for elf in elves])

def top_three_elves_calories(elves):
    return sum(list(sorted((sum(elf) for elf in elves), reverse=True))[:3])

class CalorieCounting:
    def part1(self, text):
        return max_elf_calories(parse_elves(text))

    def part2(self, text):
        return top_three_elves_calories(parse_elves(text))
