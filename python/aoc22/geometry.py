from dataclasses import dataclass
from enum import Enum
from itertools import product
from functools import total_ordering
from typing import Iterable, Self


class Rotation(Enum):
    ID = [[1, 0], [0, 1]]
    CLOCKWISE = [[0, -1], [1, 0]]
    COUNTERCLOCKWISE = [[0, 1], [-1, 0]]
    HALF_TURN = [[-1, 0], [0, -1]]

    def __iter__(self):
        return iter(self.value)


class Direction(Enum):
    U = (0, -1)
    D = (0, 1)
    L = (-1, 0)
    R = (1, 0)

    def __iter__(self):
        return iter(self.value)

    def __rmul__(self, other: Rotation) -> Self:
        if isinstance(other, Rotation):
            return Direction(tuple(sum(x_i * r_ij for x_i, r_ij in zip(self, r_j)) for r_j in other))
        else:
            return NotImplemented

@dataclass
@total_ordering
class Point:
    coords: list[int]

    def __init__(self, *coords):
        if len(coords) == 1 and not isinstance(coords, int):
            self.coords = list(coords[0])
        else:
            self.coords = list(coords)

    @property
    def x(self) -> int:
        return self[0]

    @property
    def y(self) -> int:
        return self[1]

    def __add__(self, other: Direction|Self) -> Self:
        return Point(
            x_i + y_i
            for x_i, y_i in zip(self, other)
        )

    def __sub__(self, other: Direction|Self) -> Self:
        return Point(
            x_i - y_i
            for x_i, y_i in zip(self, other)
        )

    def __mul__(self, n: int|Self) -> Self:
        if isinstance(other, Point):
            return Point(x_i * y_i for x_i, y_i in zip(self, other))
        else:
            return Point(n * x_i for x_i in self)

    def __rmul__(self, other: int|Self|Rotation) -> Self:
        if isinstance(other, Point):
            return Point(x_i * y_i for x_i, y_i in zip(self, other))
        elif isinstance(other, Rotation):
            return Point(sum(x_i * r_ij for x_i, r_ij in zip(self, r_j)) for r_j in other)
        else:
            return Point(n * x_i for x_i in self)

    def norm(self) -> int:
        return sum(abs(x_i) for x_i in self)

    def __lt__(self, other: Self) -> bool:
        return all(
            x_i < y_i
            for x_i, y_i in zip(self, other)
        )

    def __le__(self, other: Self) -> bool:
        return all(
            x_i <= y_i
            for x_i, y_i in zip(self, other)
        )

    def is_adjacent_to(self, other: Self) -> bool:
        return all(
            abs(x_i - y_i) <= 1
            for x_i, y_i in zip(self, other)
        )

    def __iter__(self):
        return iter(self.coords)

    def __getitem__(self, i) -> int:
        return self.coords[i]

    def __hash__(self) -> int:
        return hash(tuple(self.coords))

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({', '.join(map(repr, self.coords))})"

@dataclass
class Segment:
    start: Point
    end: Point

    def length(self) -> int:
        return (self.start - self.end).norm()

    def __and__(self, other: Self) -> Point|None:
        t = (
            (self.start.x - other.start.x) * (other.start.y - other.end.y)
            - (self.start.y - other.start.y) * (other.start.x - other.end.x)
        )
        u = (
            (self.start.x - other.start.x) * (self.start.y - self.end.y)
            - (self.start.y - other.start.y) * (self.start.x - self.end.x)
        )
        d = (
            (self.start.x - self.end.x) * (other.start.y - other.end.y)
            - (self.start.y - self.end.y) * (other.start.x - other.end.x)
        )

        s = copysign(1, d)

        if d != 0 and 0 <= s * t <= s * d and 0 <= s * u <= s * d:
            return Point(
                self.start.x + t * (self.end.x - self.start.x) // d,
                self.start.y + t * (self.end.y - self.start.y) // d,
            )
        else:
            return None

    def __contains__(self, point: Point) -> bool:
        cross = (
            (point.y - self.start.y) * (self.end.x - self.start.x)
            - (point.x - self.start.x) * (self.end.y - self.start.y)
        )
        dot = (
            (point.x - self.start.x) * (self.end.x - self.start.x)
            + (point.y - self.start.y) * (self.end.y - self.start.y)
        )
        length = (self.end.x - self.start.x) ** 2 + (self.end.y - self.start.y) ** 2
        return cross == 0 and 0 <= dot <= length

    def points(self):
        if self.start.x == self.end.x:
            return (
                Point(self.start.x, y)
                for y in range(min(self.start.y, self.end.y), max(self.start.y, self.end.y)+1)
            )
        elif self.start.y == self.end.y:
            return (
                Point(x, self.start.y)
                for x in range(min(self.start.x, self.end.x), max(self.start.x, self.end.x)+1)
            )
        else:
            raise ValueError()


@dataclass(frozen=True)
class Rectangle:
    lo: Point
    hi: Point

    @classmethod
    def range(cls, x: int, y: int) -> Self:
        return cls(Point(0, 0), Point(x - 1, y - 1))

    @classmethod
    def bounds(cls, xs: list[list[int]]) -> Self:
        return cls.range(len(xs[0]), len(xs))

    @classmethod
    def bounding_box(cls, ps: list[Point]) -> Self:
        return cls(Point(min(p.x for p in ps), min(p.y for p in ps)), Point(max(p.x for p in ps), max(p.y for p in ps)))

    @classmethod
    def square(cls, lo: Point, l: int) -> Self:
        return cls(lo, Point(lo.x + l - 1, lo.y + l - 1))

    def __contains__(self, point: Point) -> bool:
        return self.lo <= point <= self.hi

    def points(self):
        return map(Point, product(range(lo.x, hi.x+1), range(lo.y, hi.y+1)))

    @property
    def area(self) -> int:
        return (self.hi.x - self.lo.x + 1) * (self.hi.y - self.lo.y + 1)
