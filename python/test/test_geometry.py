import pytest

from aoc22.geometry import Rotation, Direction, Point, Segment


class TestDirection:
    @pytest.mark.parametrize("direction, expected", [
        (Direction.R, Direction.D),
        (Direction.D, Direction.L),
        (Direction.L, Direction.U),
        (Direction.U, Direction.R),
    ])
    def test_rotate_clockwise(self, expected: Direction, direction: Direction):
        assert expected == direction.rotate_clockwise()

    @pytest.mark.parametrize("direction, expected", [
        (Direction.R, Direction.U),
        (Direction.U, Direction.L),
        (Direction.L, Direction.D),
        (Direction.D, Direction.R),
    ])
    def test_rotate_counterclockwise(self, expected: Direction, direction: Direction):
        assert expected == direction.rotate_counterclockwise()

    @pytest.mark.parametrize("direction, rotation, expected", [
        (Direction.R, Rotation.CLOCKWISE, Direction.D),
        (Direction.D, Rotation.CLOCKWISE, Direction.L),
        (Direction.L, Rotation.CLOCKWISE, Direction.U),
        (Direction.U, Rotation.CLOCKWISE, Direction.R),
        (Direction.R, Rotation.COUNTERCLOCKWISE, Direction.U),
        (Direction.U, Rotation.COUNTERCLOCKWISE, Direction.L),
        (Direction.L, Rotation.COUNTERCLOCKWISE, Direction.D),
        (Direction.D, Rotation.COUNTERCLOCKWISE, Direction.R),
        (Direction.R, Rotation.HALF_TURN, Direction.L),
        (Direction.L, Rotation.HALF_TURN, Direction.R),
        (Direction.U, Rotation.HALF_TURN, Direction.D),
        (Direction.D, Rotation.HALF_TURN, Direction.U),
    ])
    def test_rotation(self, direction: Direction, rotation: Rotation, expected: Direction):
        assert expected == rotation * direction


class TestPoint:
    @pytest.mark.parametrize("expected, params", [
        (Point(0, 0), [0, 0]),
        (Point(0, 0), [(0, 0)]),
    ])
    def test_init_point(self, expected: Point, params: list):
        assert expected == Point(*params)

    @pytest.mark.parametrize("expected, p1, p2", [
        (True, Point(0,0), Point(0,0)),
        (False, Point(0,0), Point(10,10)),
        (True, Point(0,0), Point(1,0)),
        (True, Point(0,0), Point(0,-1)),
        (True, Point(0,0), Point(1,-1)),
        (False, Point(0,0), Point(0,2)),
        (False, Point(0,0), Point(1,2)),
    ])
    def test_point_adjacent(self, expected: bool, p1: Point, p2: Point):
        assert p1.is_adjacent_to(p2) == expected

    @pytest.mark.parametrize("p, r, expected", [
        (Point(0, 0), Rotation.CLOCKWISE, Point(0, 0)),
        (Point(0, 0), Rotation.COUNTERCLOCKWISE, Point(0, 0)),
        (Point(0, 0), Rotation.HALF_TURN, Point(0, 0)),
        (Point(1, 0), Rotation.CLOCKWISE, Point(0, 1)),
        (Point(3, 2), Rotation.CLOCKWISE, Point(-2, 3)),
        (Point(3, 2), Rotation.COUNTERCLOCKWISE, Point(2, -3)),
        (Point(3, 2), Rotation.HALF_TURN, Point(-3, -2)),
    ])
    def test_rotate(self, p: Point, r: Rotation, expected: Point):
        assert expected == r * p


class TestSegment:
    @pytest.mark.parametrize("expected, segment", [
        ([Point(0, 0)], Segment(Point(0, 0), Point(0, 0))),
        ([Point(0, 0), Point(1, 0), Point(2, 0)], Segment(Point(0, 0), Point(2, 0))),
        ([Point(0, 0), Point(0, 1), Point(0, 2)], Segment(Point(0, 0), Point(0, 2))),
        ([Point(-2, 0), Point(-1, 0), Point(0, 0)], Segment(Point(0, 0), Point(-2, 0))),
        ([Point(0, -2), Point(0, -1), Point(0, 0)], Segment(Point(0, 0), Point(0, -2))),
    ])
    def test_points(self, expected, segment):
        assert expected == list(segment.points())
