mod calorie_counting;
mod no_space_left_on_device;
mod rock_paper_scissors;
mod tuning_trouble;

use anyhow::Result;
use clap::Parser;
use std::fs::read_to_string;

use crate::calorie_counting::{max_elf_calories, parse_elves, top_three_elves_calories};
use crate::no_space_left_on_device::Filesystem;
use crate::rock_paper_scissors::{parse_strategy_guide, score_game};
use crate::tuning_trouble::{message_start_index, packet_start_index};

#[derive(Parser, Debug)]
#[command()]
struct Args {
    #[arg(short, long)]
    day: usize,
    #[arg()]
    input: String,
}

fn main() -> Result<()> {
    let args = Args::parse();

    let input = read_to_string(args.input)?;

    match args.day {
        1 => {
            let elves = parse_elves(&input);
            println!("Part 1: {}", max_elf_calories(&elves));
            println!("Part 2: {}", top_three_elves_calories(&elves));
        }
        2 => {
            let strategy_guide = parse_strategy_guide(&input);
            println!("Part 2: {}", score_game(&strategy_guide));
        }
        6 => {
            let datastream = input;
            println!("Part 1: {:?}", packet_start_index(&datastream));
            println!("Part 2: {:?}", message_start_index(&datastream));
        }
        7 => {
            let filesystem: Filesystem = input.parse()?;
            println!("Part 1: {}", filesystem.small_directories_size());
            println!("Part 2: {}", filesystem.delete_directory_size().unwrap());
        }
        _ => {
            unimplemented!();
        }
    }
    Ok(())
}
