use itertools::Itertools;
use std::collections::HashSet;

fn is_packet_start_marker(chars: &[char]) -> bool {
    let s: HashSet<char> = chars.iter().cloned().collect();

    s.len() == 4
}

pub fn packet_start_index(datastream: &str) -> Option<usize> {
    datastream
        .chars()
        .collect_vec()
        .windows(4)
        .enumerate()
        .find(|(_, w)| is_packet_start_marker(w))
        .map(|(i, _)| i + 4)
}

fn is_message_start_marker(chars: &[char]) -> bool {
    let s: HashSet<char> = chars.iter().cloned().collect();

    s.len() == 14
}

pub fn message_start_index(datastream: &str) -> Option<usize> {
    datastream
        .chars()
        .collect_vec()
        .windows(14)
        .enumerate()
        .find(|(_, w)| is_message_start_marker(w))
        .map(|(i, _)| i + 14)
}

#[cfg(test)]
mod test {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case(Some(7), "mjqjpqmgbljsphdztnvjfqwrcgsmlb")]
    #[case(Some(5), "bvwbjplbgvbhsrlpgdmjqwftvncz")]
    #[case(Some(6), "nppdvjthqldpwncqszvftbrmjlhg")]
    #[case(Some(10), "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")]
    #[case(Some(11), "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")]
    #[case(None, "")]
    #[case(None, "abcabcabcabcabcabcabcabc")]
    fn example_part1(#[case] expected: Option<usize>, #[case] datastream: &str) {
        let actual = packet_start_index(datastream);

        assert_eq!(expected, actual);
    }

    #[rstest]
    #[case(Some(19), "mjqjpqmgbljsphdztnvjfqwrcgsmlb")]
    #[case(Some(23), "bvwbjplbgvbhsrlpgdmjqwftvncz")]
    #[case(Some(23), "nppdvjthqldpwncqszvftbrmjlhg")]
    #[case(Some(29), "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg")]
    #[case(Some(26), "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw")]
    #[case(None, "")]
    #[case(None, "abcabcabcabcabcabcabcabc")]
    fn example_part2(#[case] expected: Option<usize>, #[case] datastream: &str) {
        let actual = message_start_index(datastream);

        assert_eq!(expected, actual);
    }
}
