use std::cmp::Reverse;

pub fn parse_elves(text: &str) -> Vec<Vec<u32>> {
    text.split("\n\n")
        .map(|elf| {
            elf.split("\n")
                .filter(|item| item != &"")
                .map(|item| item.parse().unwrap())
                .collect()
        })
        .collect()
}

pub fn max_elf_calories(elves: &Vec<Vec<u32>>) -> u32 {
    elves
        .iter()
        .map(|elf| elf.iter().sum())
        .max()
        .unwrap_or_default()
}

pub fn top_three_elves_calories(elves: &Vec<Vec<u32>>) -> u32 {
    let mut calories: Vec<u32> = elves.iter().map(|elf| elf.iter().sum()).collect();
    calories.sort_by_key(|&item| Reverse(item));
    calories.into_iter().take(3).sum()
}

#[cfg(test)]
mod test {
    use super::{max_elf_calories, parse_elves, top_three_elves_calories};

    const TEXT: &str = r#"1000
2000
3000

4000

5000
6000

7000
8000
9000

10000"#;

    #[test]
    fn example_part1() {
        let actual = max_elf_calories(&parse_elves(TEXT));

        assert_eq!(24000, actual);
    }

    #[test]
    fn example_part2() {
        let actual = top_three_elves_calories(&parse_elves(TEXT));

        assert_eq!(45000, actual);
    }
}
