use itertools::Itertools;
use std::collections::HashMap;

pub fn parse_strategy_guide(text: &str) -> Vec<(char, char)> {
    text.split("\n")
        .map(|round| {
            round
                .split(" ")
                .map(|s| s.chars().next().unwrap())
                .collect_tuple()
                .unwrap()
        })
        .collect()
}

pub fn score_game(rounds: &Vec<(char, char)>) -> u32 {
    let results: HashMap<(char, char), u32> = vec![
        (('A', 'X'), 3),
        (('A', 'Y'), 4),
        (('A', 'Z'), 8),
        (('B', 'X'), 1),
        (('B', 'Y'), 5),
        (('B', 'Z'), 9),
        (('C', 'X'), 2),
        (('C', 'Y'), 6),
        (('C', 'Z'), 7),
    ]
    .into_iter()
    .collect();

    rounds.iter().map(|round| results[round]).sum()
}

#[cfg(test)]
mod test {
    use super::*;

    const TEXT: &str = r#"A Y
B X
C Z"#;

    #[test]
    fn example_part2() {
        let actual = score_game(&parse_strategy_guide(TEXT));

        assert_eq!(12, actual);
    }
}
