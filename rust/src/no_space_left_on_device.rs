use std::collections::HashMap;
use std::str::FromStr;
use std::num::ParseIntError;
use std::cmp::min;
use thiserror::Error;

#[derive(Debug, Error)]
pub enum Error {
    #[error("Parse error")]
    ParseError(String),
    #[error("Expected an integer")]
    ParseIntError(#[from] ParseIntError),
    #[error("{0} does not exist")]
    DoesNotExist(String),
    #[error("{0} is not a directory")]
    NotADirectory(String),
}

#[derive(Debug, PartialEq)]
pub enum DirectoryEntry {
    File(File),
    Directory(Directory),
}

#[derive(Debug, PartialEq)]
pub struct Directory {
    content: HashMap<String, DirectoryEntry>,
}

impl Directory {
    fn new() -> Self {
        Self { content: HashMap::new() }
    }

    fn size(&self) -> usize {
        self.content.iter().map(|(_, entry)| entry.size()).sum()
    }

    fn small_directories_size(&self) -> usize {
        Some(self.size()).filter(|size| *size <= 100_000).unwrap_or_default() +
        self.content
            .iter()
            .map(|(_, entry)| entry.small_directories_size())
            .sum::<usize>()
    }

    fn delete_directory_size(&self, required: usize) -> Option<usize> {
        let size = self.size();
        if size >= required {
            self.content
                .iter()
                .filter_map(|(_, entry)| {
                    match entry {
                        DirectoryEntry::File(_) => None,
                        DirectoryEntry::Directory(d) => Some(d),
                    }
                })
                .filter_map(|directory| directory.delete_directory_size(required))
                .min()
                .or(Some(size))
        } else {
            None
        }
    }

    fn get_mut(&mut self, name: &str) -> Result<&mut DirectoryEntry, Error> {
        self.content.get_mut(name).ok_or(Error::DoesNotExist(name.to_string()))
    }

    fn insert(&mut self, name: String, entry: DirectoryEntry) {
        self.content.insert(name, entry);
    }
}

impl FromIterator<(String, DirectoryEntry)> for Directory {
    fn from_iter<T>(iter: T) -> Self
        where T: IntoIterator<Item = (String, DirectoryEntry)>
    {
        Self {
            content: HashMap::from_iter(iter),
        }
    }
}

#[derive(Debug, PartialEq)]
pub struct Filesystem {
    root: Directory,
}

#[derive(Debug, PartialEq)]
pub struct File {
    size: usize,
}

impl DirectoryEntry {
    fn size(&self) -> usize {
        match self {
            DirectoryEntry::File(file) => file.size,
            DirectoryEntry::Directory(directory) => directory.size(),
        }
    }

    fn small_directories_size(&self) -> usize {
        match self {
            DirectoryEntry::File(_) => 0,
            DirectoryEntry::Directory(directory) => directory.small_directories_size(),
        }
    }
}

impl Filesystem {
    pub fn new() -> Self {
        Self { root: Directory::new() }
    }

    pub fn insert(
        &mut self,
        path: &[&str],
        name: String,
        entry: DirectoryEntry,
    ) -> Result<(), Error> {
        let mut current_directory = &mut self.root;

        for segment in path {
            current_directory = match current_directory.get_mut(*segment)? {
                DirectoryEntry::File(_) => {
                    return Err(Error::NotADirectory(segment.to_string()))
                }
                DirectoryEntry::Directory(ref mut directory) => directory,
            }
        }

        current_directory.insert(name, entry);
        Ok(())
    }

    fn size(&self) -> usize {
        self.root.size()
    }

    pub fn small_directories_size(&self) -> usize {
        self.root.small_directories_size()
    }

    pub fn delete_directory_size(&self) -> Option<usize> {
        let unused_size = 70_000_000 - self.size();
        let required = 30_000_000 - dbg!(unused_size);

        self.root.delete_directory_size(required)
    }
}

impl FromIterator<(String, DirectoryEntry)> for Filesystem {
    fn from_iter<T>(iter: T) -> Self
        where T: IntoIterator<Item = (String, DirectoryEntry)>
    {
        Self {
            root: Directory::from_iter(iter)
        }
    }
}

impl FromStr for Filesystem {
    type Err = Error;

    fn from_str(text: &str) -> Result<Self, Self::Err> {
        let mut result = Filesystem::new();

        let mut path = Vec::new();

        for line in text.lines() {
            if line.starts_with("$ cd") {
                if line == "$ cd /" {
                    path = Vec::new();
                } else if line == "$ cd .." {
                    path.pop();
                } else {
                    path.push(&line[5..]);
                }
            } else if line.starts_with("$ ls") {
            } else if line.starts_with("dir") {
                let terms: Vec<&str> = line.split(" ").collect();
                result.insert(
                    &path,
                    terms[1].to_string(),
                    DirectoryEntry::Directory(Directory {
                        content: HashMap::new(),
                    }),
                )?;
            } else {
                let terms: Vec<&str> = line.split(" ").collect();
                result.insert(
                    &path,
                    terms[1].to_string(),
                    DirectoryEntry::File(File {
                        size: terms[0].parse()?,
                    }),
                )?;
            }
        }

        Ok(result)
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use indoc::indoc;
    use rstest::{fixture, rstest};

    #[rstest]
    fn parse_one_file() {
        let example = indoc!(
            r#"
            $ cd /
            $ ls
            584 i
        "#
        );

        let actual: Filesystem = example.parse().unwrap();

        let expected: Filesystem = vec![("i".to_string(), DirectoryEntry::File(File { size: 584 }))]
                .into_iter()
                .collect();

        assert_eq!(expected, actual);
    }

    #[rstest]
    fn parse_one_dir_with_one_file() {
        let example = indoc!(
            r#"
            $ cd /
            $ ls
            dir e
            $ cd e
            $ ls
            584 i
        "#
        );

        let actual: Filesystem = example.parse().unwrap();

        let expected: Filesystem = vec![(
                    "e".to_string(),
                    DirectoryEntry::Directory(
                        vec![("i".to_string(), DirectoryEntry::File(File { size: 584 }))]
                            .into_iter()
                            .collect()),
                )]
                .into_iter()
                .collect();

        assert_eq!(expected, actual);
    }

    #[fixture]
    fn example() -> &'static str {
        indoc!(
            r#"
            $ cd /
            $ ls
            dir a
            14848514 b.txt
            8504156 c.dat
            dir d
            $ cd a
            $ ls
            dir e
            29116 f
            2557 g
            62596 h.lst
            $ cd e
            $ ls
            584 i
            $ cd ..
            $ cd ..
            $ cd d
            $ ls
            4060174 j
            8033020 d.log
            5626152 d.ext
            7214296 k
        "#)
    }

    #[rstest]
    fn example_part1(example: &str) {
        let filesystem: Filesystem = example.parse().unwrap();

        let actual = filesystem.small_directories_size();

        assert_eq!(95437, actual);
    }

    #[rstest]
    fn example_part2(example: &str) {
        let filesystem: Filesystem = example.parse().unwrap();

        let actual = filesystem.delete_directory_size().unwrap();

        assert_eq!(24933642, actual);
    }
}
